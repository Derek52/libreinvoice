package xyz.libreinvoice.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "clients")
data class Client(
    @field:ColumnInfo(name = "client_name")var name : String = "Empty",
    @field:ColumnInfo(name = "client_email")var email : String = "Empty",
    @field:ColumnInfo(name = "client_phone_number")var number : String = "Empty"
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")var id : Long = 0
}