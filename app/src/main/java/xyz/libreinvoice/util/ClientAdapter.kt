package xyz.libreinvoice.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import xyz.libreinvoice.R
import xyz.libreinvoice.database.Client

class ClientAdapter(private val clients : List<Client>,
                    private val context : Context) :
RecyclerView.Adapter<ClientAdapter.ClientViewHolder>() {

    lateinit var clickListener: ClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_client, parent, false)
        return ClientViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClientViewHolder, position: Int) {
        val client = clients[position]

        holder.nameView.text = client.name
        holder.emailView.text = client.email
        holder.numberView.text = client.number

        holder.newInvoiceButton.setOnClickListener {

        }
    }

    override fun getItemCount(): Int {
        return clients.size
    }

    fun setOnItemClickListener(listener: ClickListener) {
        clickListener = listener
    }

    inner class ClientViewHolder(view : View) : RecyclerView.ViewHolder(view),
            View.OnClickListener, View.OnLongClickListener {
        internal val nameView = view.findViewById<TextView>(R.id.itemClientName)
        internal val emailView = view.findViewById<TextView>(R.id.itemClientEmail)
        internal val numberView = view.findViewById<TextView>(R.id.itemClientNumber)
        internal val newInvoiceButton = view.findViewById<TextView>(R.id.clientItemNewInvoice)
        //internal val itemClientInnerLayout = view.findViewById<LinearLayout>(R.id.item_client_inner_layout)

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view : View) {
            clickListener.onItemClick(adapterPosition, view)
        }

        override fun onLongClick(view: View): Boolean {
            clickListener.onItemLongClick(adapterPosition, view)
            return true
        }
    }
}