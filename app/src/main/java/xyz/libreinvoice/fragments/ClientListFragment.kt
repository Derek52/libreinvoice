package xyz.libreinvoice.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_client_list.*
import kotlinx.android.synthetic.main.fragment_client_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import xyz.libreinvoice.R
import xyz.libreinvoice.database.AppDatabase
import xyz.libreinvoice.database.Client
import xyz.libreinvoice.database.ClientDao
import xyz.libreinvoice.util.ClickListener
import xyz.libreinvoice.util.ClientAdapter
import xyz.libreinvoice.viewmodels.ClientViewModel

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ClientListFragment : Fragment() {

    lateinit var model : ClientViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_client_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this)[ClientViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        GlobalScope.launch {
            getClients()
        }

        clientListFab.setOnClickListener {
            findNavController().navigate(R.id.action_add_clients)
        }
    }

    override fun onResume() {
        super.onResume()
        GlobalScope.launch {
            getClients()
        }
    }

    private fun getClientDao() : ClientDao {
        val db : AppDatabase = AppDatabase.getDatabase(this@ClientListFragment.requireContext())
        return db.clientDao()
    }

    private suspend fun getClients() =
        withContext(Dispatchers.IO) {
            val clientDao = getClientDao()
            val clients = clientDao.clients
            model.clients = clients
            this@ClientListFragment.activity?.runOnUiThread {
                showClients(clients)
            }
        }

    private fun deleteClient(id : Long) {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                val clientDao = getClientDao()
                clientDao.deleteClientById(id)
                getClients()
            }
        }
    }
    private fun showClients(clients : List<Client>) {
        val clientAdapter = ClientAdapter(clients, this.requireContext())
        clientAdapter.setOnItemClickListener(object : ClickListener {
            override fun onItemClick(position : Int, view : View) {
                val client = clients[position]
                model.selectedClient = client
                findNavController().navigate(R.id.action_show_client)
            }

            override fun onItemLongClick(position : Int, view : View) {
                deleteClient(clients[position].id)
            }
        })

        clientRecyclerView.setHasFixedSize(false)
        val layoutManager = LinearLayoutManager(this.requireContext())
        clientRecyclerView.layoutManager = layoutManager
        clientRecyclerView.addItemDecoration(
            DividerItemDecoration(this.requireContext(), LinearLayoutManager.VERTICAL))
        clientRecyclerView.itemAnimator = DefaultItemAnimator()
        clientRecyclerView.adapter = clientAdapter
    }
}
